# Matrix multiplying
Hello, this is the app for matrix multiplying. In CMD you can change the way you will count.
Example for input of matrices is in file input.txt. For using input file, move it to build folder.
To input right in the CMD, write the values
raw by raw using the space between the values.
For example:

- Enter amount of raws - 2
- Enter amount of columns - 2
- 1 2
- 3 4


- If you want to print the solution right to cmd, use command <direct_output>
- If you want to print the solution to the file, use command <file_output> 
- If you want to fill matrix from cmd, use command <direct_input> 
- If you want to fill matrix from the file, use command <file_input> 
- Default output is result.txt, for changing use command <output_path yourFile.txt> 
- Default input is input.txt, for changing use command <input_path yourFile.txt> 
- Also, you can set the mode MultiThread or SingleThread 
- Use <multi_thread> or <single_thread> 
- To start counting type <start>


The input is 120x120 matrix multiplied the same matrix. It is that big to show the difference
between single_thread mode and multi_thread mode, that you can set in the cmd, when the program is started.

//
// Created by miron on 29.01.2021.
//

#include <iostream>
#include <conio.h>
#include <fstream>
#include "../headers/Counter.hpp"
#include "My_exceptions.cpp"
#include <chrono>

template <typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

Matrix Counter::multiply(std::vector<Matrix *> &matrices) {
    auto start = std::chrono::high_resolution_clock::now();

    Matrix *first = matrices.at(0);
    Matrix *second = matrices.at(1);
    if (first->getColumns() != second->getRaws()) {
        throw invalid_input_exception(
                "Number of columns of first Matrix must be the same as rows in the second matrix");
    }
    int res_raws = first->getRaws();
    int res_columns = second->getColumns();

    auto **values = new double *[res_raws];
    for (int i = 0; i < res_raws; i++) {
        values[i] = new double[res_columns];
    }

    if (!multi_thread) {
        for (int i = 0; i < res_raws; i++) {
            for (int j = 0; j < second->getRaws(); j++) {
                values[i][j] = 0;
                for (int k = 0; k < first->getColumns(); k++)
                    values[i][j] += first->getValues()[i][k] * second->getValues()[k][j];
            }
        }
    } else {

        std::thread secondCounter(countSecondHalf, matrices, values);
        for (int i = 0; i < res_raws / 2; i++) {
            for (int j = 0; j < second->getRaws(); j++) {
                values[i][j] = 0;
                for (int k = 0; k < first->getColumns(); k++)
                    values[i][j] += first->getValues()[i][k] * second->getValues()[k][j];
            }
        }
        secondCounter.join();
    }
    ////////////////////
    auto end = std::chrono::high_resolution_clock::now();
    if(multi_thread){
        std::cout << "MultiTread mode: "<< std::endl;
    }else{
        std::cout << "SingleThread mode: "<< std::endl;
    }
    std::cout << "Needed " << to_ms(end - start).count() << " ms to finish.\n";
    ////////////////////
    return Matrix(res_raws, res_columns, values);
}

Matrix* Counter::readMatrixFromCmd() {
    int raws;
    int columns;
    std::cout << "Enter amount of raws -";
    std::cin >> raws;
    std::cout << "Enter amount of columns -";
    std::cin >> columns;

    std::cout << "Initialize the matrix (" + std::to_string(raws) + "x" + std::to_string(columns) + ")" << std::endl;
    auto **values = new double *[raws];
    for (int i = 0; i < raws; i++) {
        values[i] = new double[columns];
    }

    for (int i = 0; i < raws; i++) {
        for (int j = 0; j < columns; j++) {
            std::string expr;
            unsigned char c = _getch();
            while (!std::isspace(c)) {
                expr.push_back(c);
                c = _getch();
            }
            values[i][j] = std::stod(expr);

            std::cout << "\t";
        }
        std::cout << std::endl;
    }
    auto *matrix = new Matrix(raws, columns, values);
    return matrix;
}

void Counter::dirInputCount() {
    Matrix *matrix1 = readMatrixFromCmd();
    Matrix *matrix2 = readMatrixFromCmd();
    std::vector<Matrix *> matrices;
    matrices.push_back(matrix1);
    matrices.push_back(matrix2);

    Matrix matrix_res = multiply(matrices);
    for(auto *matrix : matrices){
        delete matrix;
    }
    writeResult(matrix_res);
}

void Counter::fileInputCount() {
    std::vector<Matrix *> matrices = readMatricesFromFile();
    Matrix matrix_res = multiply(matrices);

    writeResult(matrix_res);
}

std::vector<Matrix *> Counter::readMatricesFromFile() {
    std::ifstream f(in_path);
    if (!f) {
        throw my_exception("Couldn't read the file");
    }

    Matrix *matrix1 = readCurrentFromFile(f);
    Matrix *matrix2 = readCurrentFromFile(f);

    std::vector<Matrix *> matrices;
    matrices.push_back(matrix1);
    matrices.push_back(matrix2);
    return matrices;
}

Matrix *Counter::readCurrentFromFile(std::ifstream &file) {
    int raws;
    int columns;
    file >> raws >> columns;

    auto **values = new double *[raws];
    for (int i = 0; i < raws; i++) {
        values[i] = new double[columns];
    }

    for (int i = 0; i < raws; i++) {
        for (int j = 0; j < columns; j++) {
            try {
                std::string value;
                file >> value;
                values[i][j] = std::stod(value);
            }catch(std::exception &e){
                std::cout<<"Couldn't read the matrix" << std::endl;
            }
        }
    }
    auto *matrix = new Matrix(raws, columns, values);
    return matrix;
}

void Counter::writeResult(Matrix &matrix) const {
    if (direct_output) {
        std::cout << "Result :" << std::endl;
        matrix.print(std::cout);
    } else {
        std::ofstream result(out_path);
        matrix.print(result);
    }
}

void Counter::start() {
    if (direct_input) {
        dirInputCount();
    } else {
        fileInputCount();
    }
}

void Counter::countSecondHalf(const std::vector<Matrix *> &matrices, double **values) {
    Matrix *first = matrices.at(0);
    Matrix *second = matrices.at(1);
    for (int i = first->getRaws() / 2; i < first->getRaws(); i++) {
        for (int j = 0; j < second->getRaws(); j++) {
            values[i][j] = 0;
            for (int k = 0; k < first->getColumns(); k++)
                values[i][j] += first->getValues()[i][k] * second->getValues()[k][j];
        }
    }
}

const std::string &Counter::getOutPath() const {
    return out_path;
}

void Counter::setOutPath(const std::string &outPath) {
    out_path = outPath;
}

const std::string &Counter::getInPath() const {
    return in_path;
}

void Counter::setInPath(const std::string &inPath) {
    in_path = inPath;
}

bool Counter::isDirectOutput() const {
    return direct_output;
}

void Counter::setDirectOutput(bool directOutput) {
    direct_output = directOutput;
}

bool Counter::isMultiThread() const {
    return multi_thread;
}

void Counter::setMultiThread(bool multiThread) {
    multi_thread = multiThread;
}

bool Counter::isDirectInput() const {
    return direct_input;
}

void Counter::setDirectInput(bool directInput) {
    direct_input = directInput;
}


//
// Created by miron on 29.01.2021.
//

#ifndef MATRIX_MULTIPLYING_COUNTER_HPP
#define MATRIX_MULTIPLYING_COUNTER_HPP

#include <string>
#include "Matrix.hpp"
#include <thread>
class Counter {
private:
    std::string in_path = "input.txt";
    std::string out_path = "result.txt";
    bool direct_output = false;
    bool direct_input = false;
    bool multi_thread = false;

public:
    void writeResult(Matrix &matrix) const;

    static Matrix* readMatrixFromCmd();

    Matrix multiply(std::vector<Matrix*> &matrices);

    static Matrix * readCurrentFromFile(std::ifstream &file);

    std::vector<Matrix *> readMatricesFromFile();

    static void countSecondHalf(const std::vector<Matrix *> &matrices, double **values);

    void dirInputCount();

    const std::string &getOutPath() const;

    void setOutPath(const std::string &outPath);

    const std::string &getInPath() const;

    void setInPath(const std::string &inPath);

    bool isDirectOutput() const;

    void setDirectOutput(bool directOutput);

    bool isMultiThread() const;

    void setMultiThread(bool multiThread);

    bool isDirectInput() const;

    void setDirectInput(bool directInput);

    void fileInputCount();

    void start();
};


#endif //MATRIX_MULTIPLYING_COUNTER_HPP

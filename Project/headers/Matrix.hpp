//
// Created by miron on 29.01.2021.
//

#ifndef MATRIX_MULTIPLYING_MATRIX_HPP
#define MATRIX_MULTIPLYING_MATRIX_HPP
#include <vector>

class Matrix {
private:
    int raws;
    int columns;
    double **values;
public:
    Matrix(int init_raws, int init_columns, double **init_values);

    Matrix();

    ~Matrix();

    int getRaws() const;

    int getColumns() const;

    double **getValues() const;

    void setValues(double **init_values);

    void print(std::ostream &os) const;
};


#endif //MATRIX_MULTIPLYING_MATRIX_HPP

#include <iostream>
#include <vector>
#include "../headers/CmdReader.hpp"

int main(int argc, char *argv[]) {
    std::string current_exec_name = argv[0];
    std::vector<std::string> all_args;

    if (argc > 1) {
        all_args.assign(argv + 1, argv + argc);
    }

    for (const std::string &s :all_args) {
        std::cout << s;
    }

    auto *counter = new Counter();
    auto *cmdReader = new CmdReader(*counter);

    cmdReader->readCommand();

}
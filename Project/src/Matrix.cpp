//
// Created by miron on 29.01.2021.
//

#include <iostream>
#include "../headers/Matrix.hpp"

Matrix::Matrix(int init_raws, int init_columns, double **init_values) {
    this->raws = init_raws;
    this->columns = init_columns;
    this->values = init_values;
}

int Matrix::getRaws() const {
    return raws;
}

int Matrix::getColumns() const {
    return columns;
}

double **Matrix::getValues() const {
    return values;
}

void Matrix::setValues(double **init_values) {
    Matrix::values = init_values;
}

Matrix::~Matrix() {
    for (int i = 0; i < raws; i++) {
        delete[] values[i];
    }
    delete[] values;
}

void Matrix::print(std::ostream &os) const {
    os << "-----------\n";
    for (int i = 0; i < raws; i++) {
        for (int j = 0; j < columns; j++) {
            os << values[i][j] << "\t";
        }
        os << std::endl;
    }
    os << "-----------\n";
}

Matrix::Matrix() {

}



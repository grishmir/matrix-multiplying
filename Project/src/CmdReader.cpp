//
// Created by miron on 29.01.2021.
//

#include <iostream>
#include <conio.h>
#include "../headers/CmdReader.hpp"
#include "My_exceptions.cpp"

CmdReader::CmdReader(Counter &counter) {
    this->counter = counter;
    std::cout
            << "\n-------------------------------------------------------------------------------------------------------\n";
    std::cout << "\nHello, this is the app for matrix multiplying \n"
                 "here you can change the way you will count. \n"
                 "If you want to print the solution right to cmd, use command <direct_output> \n"
                 "If you want to print the solution to the file, use command <file_output> \n"
                 "If you want to fill matrix from cmd, use command <direct_input> \n"
                 "If you want to fill matrix from the file, use command <file_input> \n"
                 "Default output is result.txt, for changing use command <output_path yourFile.txt> \n"
                 "Default input is input.txt, for changing use command <input_path yourFile.txt> \n"
                 "Also, you can set the mode MultiThread or SingleThread \n"
                 "Use <multi_thread> or <single_thread> \n"
                 "To start counting type <start>"
                 "\n-------------------------------------------------------------------------------------------------------\n";
}

void CmdReader::readCommand() {
    std::string line;
    while (std::getline(std::cin, line)) {
        try {
            handle_line(std::cout, line);
        } catch (my_exception &e) {
            std::cout << e.what();
        }
        std::cout << std::endl;
    }
}

void CmdReader::handle_line(std::ostream &os, std::string &line) {
    if (line == "direct_output") {
        counter.setDirectOutput(true);
    } else if (line == "file_output") {
        counter.setDirectOutput(false);
    } else if (line == "direct_input") {
        counter.setDirectInput(true);
    } else if (line == "file_input") {
        counter.setDirectInput(false);
    } else if (line.rfind("output_path", 0) != -1) {
        std::string out_path = erase_value(line);
        counter.setOutPath(out_path);
    } else if (line.rfind("input_path", 0) != -1) {
        std::string in_path = erase_value(line);
        counter.setInPath(in_path);
    } else if (line == "multi_thread") {
        counter.setMultiThread(true);
    } else if (line == "single_thread") {
        counter.setMultiThread(false);
    } else if (line == "start") {
        counter.start();
    } else if (line == "--help") {
        os << "All possible commands: \n"
              "direct_output \n"
              "file_output \n"
              "output_path yourFile.txt \n"
              "input_path yourFile.txt \n"
              "multi_thread \n"
              "single_thread \n"
              "start \n";
    } else {
        throw invalid_command("Unknown command " + line + "! Type --help for the list of commands");
    }
}


std::string CmdReader::erase_value(std::string &line) {
    std::vector<std::string> tokens;
    std::stringstream check(line);
    std::string temp;
    while (getline(check, temp, ' ')) {
        tokens.push_back(temp);
    }
    std::string erased = tokens[1];
    return erased;
}

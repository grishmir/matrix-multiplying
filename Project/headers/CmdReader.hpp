//
// Created by miron on 29.01.2021.
//

#ifndef MATRIX_MULTIPLYING_CMDREADER_HPP
#define MATRIX_MULTIPLYING_CMDREADER_HPP
#include <sstream>
#include <vector>
#include <string>
#include "Counter.hpp"
#include "Matrix.hpp"

struct CmdReader {
    Counter counter;

    CmdReader(Counter &init_counter);

    void readCommand();


    void handle_line(std::ostream &os, std::string &line);

    static std::string erase_value(std::string &line);
};

#endif //MATRIX_MULTIPLYING_CMDREADER_HPP

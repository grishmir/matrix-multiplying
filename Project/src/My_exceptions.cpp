//
// Created by miron on 30.01.2021.
//

#include <exception>
#include <string>

class my_exception : public std::exception {
public:
    explicit my_exception(std::string s) : text(std::move(s)) {}

    const char *what() const noexcept override {
        return text.c_str();
    }

protected:
    std::string text{};
};

class parse_exception : public my_exception {
public:
    using my_exception::my_exception;
};

class invalid_input_exception : public my_exception {
public:
    using my_exception::my_exception;
};

class invalid_command : public my_exception {
public:
    using my_exception::my_exception;
};